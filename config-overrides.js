const {fixBabelImports, addLessLoader, override} = require('customize-cra');
const _ = require('lodash');
const scssToJson = require('scss-to-json');

const scssThemeVariables = scssToJson('./src/varsAntD.scss');
const themeVariables = _.mapKeys(scssThemeVariables, (value, key) => key.replace(/^\$/, '@'));

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  fixBabelImports('lodash', {
    libraryDirectory: '',
    camel2DashComponentName: false
  }),
  addLessLoader({
    lessOptions: {
      javascriptEnabled: true,
      modifyVars: themeVariables
    }
  })
);
